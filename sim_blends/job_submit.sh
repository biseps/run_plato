#!/bin/bash


define_inputs() {

    # setup most basic locations
    USER_DIR="/padata/beta/users/pmr257"
    ROOT_DIR="$USER_DIR/data/plato"


    # set '$RUN_PATH' to 
    # location of this script
    this_script=$(readlink -f "$0")
    RUN_PATH=$(dirname "$this_script")


    # location of source-code repository
    CODE_DIR="$USER_DIR/repos/biseps"


    # use which PLATO population data?
    # PLATO_POP_DIR="$USER_DIR/data/github_plato/pop_output/field1/run_8" # centre square 1x1 degree
    # PLATO_POP_DIR="$ROOT_DIR/pop_output/field1/run_9" # central stripe 1x1 degree
    PLATO_POP_DIR="/padata/beta/users/efarrell/data/old_platos/github_plato/pop_output/field1/run_8"



    # sqlite databases holding transit depths
    DATABASE_DIR="$ROOT_DIR/jkt_output/run_2" 


    # Where do output files go?
    OUTPUT_DIR_BASE="$ROOT_DIR/blend_output"


    # Misc constants
    BINARY_FOLDER='pop_b_y'


    # how many times to run the simulations?
    NUM_SIMULATIONS=1
    NUM_SIMULATIONS=20


    # Find all grid folders 
    # inside '$PLATO_POP_DIR'
    # target_pattern="*$BINARY_FOLDER/*"

    # Find only grid square 16
    target_pattern="*$BINARY_FOLDER/16*"


    # Python programs needed for this script
    program_simulate="$CODE_DIR/plato_sims/sim_blends.py"
    program_analyse="$CODE_DIR/plato_sims/analyse_blends.py"

}



main() {


    # Store command-line arguments
    # for use in other functions
    COMMANDLINE_ARGS=("$@")


    # Create global variables pointing
    # to input / output directories
    setup_variables


    # Save a copy 
    # to a log file
    record_job_details


    printf "\n\nSetting up Cluster Jobs. Please wait...\n"


    BATCH_NUM=1
    JOB_NUM=1


    # Find every grid square directory inside PLATO_POP_DIR
    # and create a cluster job array for each one
    find $PLATO_POP_DIR -type d -iwholename "$target_pattern" -print | while read GRID_PATH
    do

        # 'extract.1' files are stored 
        # in this directory structure:
        #
        # .../plato/data/pop_output/field1/run_6/POP_B_{O,Y}/{1..100}
        #                                                       ↑        
        #                           $GRID_SQUARE ---------------↑        



        # get square number from tip of path
        GRID_SQUARE=$(basename "$GRID_PATH")


        # print helpful info
        echo "Batch number is:        $BATCH_NUM"
        echo "Job number is:          $JOB_NUM"
        echo "Processing Grid Square: $GRID_SQUARE"
                            
                            
        # where is sqlite database?
        DATABASE="$DATABASE_DIR/$GRID_SQUARE/depths_$GRID_SQUARE.sqlite"

        
        OUTPUT_SQUARE_DIR="$RUN_OUTPUT/$GRID_SQUARE"

        # Does an output folder already 
        # exist for this grid square?
        if [ ! -d "$OUTPUT_SQUARE_DIR" ]; then
            mkdir "$OUTPUT_SQUARE_DIR"
        fi


        # How many cluster jobs should we run?
        JOB_ARRAY="1-$NUM_SIMULATIONS:1"


        # generate a name for the cluster job
        BATCH_NAME="blnd$BATCH_NUM-sq$GRID_SQUARE"


        # launch 'sim_blends.sh' as a cluster job:
        job_ids=$(qsub -terse  -hold_jid "blnd$BATCH_NUM*"    \
                    -v CODE_DIR="$CODE_DIR"                   \
                    -v RUN_OUTPUT="$RUN_OUTPUT"               \
                    -v GRID_SQUARE="$GRID_SQUARE"             \
                    -v PLATO_POP_DIR="$PLATO_POP_DIR"         \
                    -v OUTPUT_SQUARE_DIR="$OUTPUT_SQUARE_DIR" \
                    -v DATABASE="$DATABASE"                   \
                    -N ${BATCH_NAME}                          \
                    -t ${JOB_ARRAY}                           \
                    -o "$CLUSTER_MSGS"                        \
                    sim_blends.sh)


        # setup jobs to run 
        # in concurrent batches.
        # First, increment JOB_NUM by 1
        (( JOB_NUM += 1 ))

        # now should we start a 
        # new batch of jobs?
        if [ "$JOB_NUM" -gt 5 ];
        then
            (( BATCH_NUM += 1 ))
            JOB_NUM=1
            echo "new batch: $BATCH_NUM"
        fi


    done


    # when all the simulations are done,
    # run a job which creates summary
    # histograms and calculates the mean
    # of the simulation data
    job2_name="analyse_blends"

    job2_ids=$(qsub -terse -hold_jid 'blnd*'         \
               -v CODE_DIR="$CODE_DIR"               \
               -v RUN_OUTPUT="$RUN_OUTPUT"           \
               -N "$job2_name"                       \
               -o "$CLUSTER_MSGS"                    \
               analyse_blends.sh)


    # Helpful info for user
    printf "\nAll output from this job will be in folder: \n%s" "$RUN_OUTPUT"
    printf "\n\nUse 'qstat' to check on progress of this job.\n"

}



setup_variables() {

    # Optional argument:
    # Short comment explaining purpose of run.
    COMMENT=${COMMANDLINE_ARGS[0]}


    # most important step.
    # define input files, parameters, etc.
    define_inputs


    # Activate useful bash functions. If you
    # cant find a function mentioned in these
    # scripts, its probably in "utility_functions.sh"
    source "$CODE_DIR/common/utility_functions.sh"


    # where does output go?
    create_output_folder


    # check everything exists
    assert_folder_exists  "USER_DIR"       "$USER_DIR"  
    assert_folder_exists  "ROOT_DIR"       "$ROOT_DIR"  
    assert_folder_exists  "PLATO_POP_DIR"  "$PLATO_POP_DIR"  
    assert_folder_exists  "DATABASE_DIR"   "$DATABASE_DIR"   
    assert_folder_exists  "RUN_PATH"       "$RUN_PATH"       
    assert_folder_exists  "CODE_DIR"       "$CODE_DIR"       
    assert_folder_exists  "RUN_OUTPUT"     "$RUN_OUTPUT"     
    assert_folder_exists  "CLUSTER_MSGS"   "$CLUSTER_MSGS"   


    assert_file_exists    "program_simulate"        "$program_simulate"   
    assert_file_exists    "program_analyse"         "$program_analyse"   


    # copy the Python programs
    # to the runtime directory
    cp "$program_simulate"      "$RUN_OUTPUT"
    cp "$program_analyse"       "$RUN_OUTPUT"

}



create_output_folder() {

    # make sure base Output directory exists 
    if [ ! -d "$OUTPUT_DIR_BASE" ]; then 
        mkdir "$OUTPUT_DIR_BASE" --parents
    fi
       
    # Create a new directory to hold 
    # all output from this particular run. 
    RUN_OUTPUT=$(increment_folder "$OUTPUT_DIR_BASE"/run_)
    CLUSTER_MSGS="$RUN_OUTPUT/cluster_msgs"

    mkdir "$RUN_OUTPUT"
    mkdir "$CLUSTER_MSGS"
       
}



record_job_details () {

    # Store info about this job in a log file

    JOB_INFO="$RUN_OUTPUT/job_info.txt"
    LOG_FILE="$RUN_PATH/log.txt"


    echo_job_time                                             >> "$JOB_INFO"
    echo_value  'RUN_OUTPUT'         "${RUN_OUTPUT}"          >> "$JOB_INFO"
    echo_value  'COMMENT'            "${COMMENT}"             >> "$JOB_INFO"
    echo_value  'RUN_PATH'           "${RUN_PATH}"            >> "$JOB_INFO"
    echo_value  'CODE_DIR'           "${CODE_DIR}"            >> "$JOB_INFO"
    echo_value  'PLATO_POP_DIR'      "${PLATO_POP_DIR}"       >> "$JOB_INFO"
    echo_value  'DATABASE_DIR'       "${DATABASE_DIR}"        >> "$JOB_INFO"
    echo_value  'BINARY_FOLDER'      "${BINARY_FOLDER}"       >> "$JOB_INFO"
    echo_value  'NUM_SIMULATIONS'    "${NUM_SIMULATIONS}"     >> "$JOB_INFO"

    show_git_status    "$CODE_DIR/plato_sims"                 >> "$JOB_INFO" 
    show_git_status    "$CODE_DIR/common"                     >> "$JOB_INFO" 


    insert_log  "${COMMENT}"  "${RUN_OUTPUT}"   >> "$LOG_FILE"


    # make a copy of all files
    # current in the main directory
    find "$RUN_PATH" -maxdepth 1 -type f -name '*' -exec cp "{}" "$RUN_OUTPUT" \;


    # make a few copies of the log file
    cp "$LOG_FILE"  "$RUN_OUTPUT"
    cp "$LOG_FILE"  "$OUTPUT_DIR_BASE"


    # record all the 'extract' 
    # files we're going to process
    printf "\nThe following directories will be processed:\n"         >> "$JOB_INFO" 
    find $PLATO_POP_DIR -type d -iwholename "$target_pattern" -print  >> "$JOB_INFO"

}



# Validate command-line arguments
if [ "$#" -ne 1 ];
then
    echo "Error! Command-line argument missing."
    echo "Please supply a comment (in quotes) explaining the purpose of this job"
    exit 1
fi


# Kick off the job
main "$@"




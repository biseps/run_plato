#!/bin/bash

#$ -l s_rt=100:00:00,virtual_free=6G,h_vmem=12G
#$ -S /bin/bash
#$ -j y
#$ -cwd
#$ -P science
#$ -M pam.rowden27@gmail.com
#$ -m as


# get utility functions.
# e.g. assert_file_exists, debug_msg etc
source "$CODE_DIR/common/utility_functions.sh"

# main python program
program_blends="sim_blends.py"


# Setup the Python runtime env
set_python_env  anaconda  $CODE_DIR


# Check everything ok
assert_folder_exists  "OUTPUT_SQUARE_DIR"  "$OUTPUT_SQUARE_DIR"  
assert_folder_exists  "PLATO_POP_DIR"      "$PLATO_POP_DIR"      
assert_file_exists    "DATABASE"           "$DATABASE"           
assert_file_exists    "program_blends"     "$RUN_OUTPUT/$program_blends"           


echo_value 'grid square'                "$GRID_SQUARE"
echo_value 'output folder'              "$OUTPUT_SQUARE_DIR"
echo_value 'plato population folder'    "$PLATO_POP_DIR"


debug_msg "starting...  $program_blends"

$PYTHONBIN "$RUN_OUTPUT/$program_blends"  "$GRID_SQUARE"       \
                                          "$PLATO_POP_DIR"     \
                                          "$DATABASE"          \
                                          "$OUTPUT_SQUARE_DIR" \
                                          "$SGE_TASK_ID"          

debug_msg "$program_blends finished"



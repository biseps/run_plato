#!/bin/bash

# Purpose: 

# Create a histogram summary of all the simulations
# which were created by 'sim_eclipses.sh'

# Output from this script will be written
# to 'histogram_totals' and 'stats_summary' files


# Sun Grid Engline Cluster options
#$ -l s_rt=100:00:00,virtual_free=6G,h_vmem=12G
#$ -S /bin/bash
#$ -t 1-1:1
#$ -j y
#$ -cwd
#$ -P science
#$ -M enda.farrell@open.ac.uk
#$ -m as


# activate utility functions.
# e.g. assert_file_exists, debug_msg etc
source "$CODE_DIR/common/utility_functions.sh"


# define the program we're running
program_analyse="analyse_eclipses.py"


# Setup the Python runtime env
set_python_env  anaconda  $CODE_DIR


# double-check everything exists
assert_folder_exists  "RUN_OUTPUT"          "$RUN_OUTPUT"     
assert_file_exists    "program_analyse"     "$RUN_OUTPUT/$program_analyse"   


# make a note of the directory we're processing
echo_value 'output directory'  "$RUN_OUTPUT"



debug_msg "starting...  $program_analyse"

$PYTHONBIN "$RUN_OUTPUT/$program_analyse"  "$RUN_OUTPUT"

debug_msg "$program_analyse finished"



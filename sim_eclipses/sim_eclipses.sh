#!/bin/bash

# Sun Grid Engline Cluster options

#$ -l s_rt=100:00:00,virtual_free=6G,h_vmem=12G
#$ -S /bin/bash
#$ -j y
#$ -cwd
#$ -P science
#$ -M enda.farrell@open.ac.uk
#$ -m as


# activate utility functions.
# e.g. assert_file_exists, debug_msg etc
source "$CODE_DIR/common/utility_functions.sh"


# this program simulates a binary system eclipse
# (it assigns a random inclination to each binary system)
program_simulate="sim_eclipses.py"


# Setup the Python runtime env
set_python_env  anaconda  $CODE_DIR


# Check everything ok
assert_folder_exists "OUTPUT_SQUARE_DIR"   "$OUTPUT_SQUARE_DIR"  
assert_file_exists   "MAG_FILE"            "$MAG_FILE"           
assert_file_exists   "EXTRACT_FILE"        "$EXTRACT_FILE"       
assert_file_exists   "DATABASE"            "$DATABASE"           


echo_value 'output directory'  "$RUN_OUTPUT"
echo_value 'binary type '      "$BINARY_TYPE"


debug_msg "starting...  $program_simulate"

$PYTHONBIN "$RUN_OUTPUT/$program_simulate"  "$MAG_FILE"          \
                                            "$EXTRACT_FILE"      \
                                            "$DATABASE"          \
                                            > "$OUTPUT_SQUARE_DIR/simulated.$BINARY_TYPE.$SGE_TASK_ID"


debug_msg "$program_simulate finished"

#!/bin/bash                             


define_inputs() {

    # setup most basic locations
    USER_DIR="/padata/beta/users/pmr257"
    ROOT_DIR="$USER_DIR/data/plato"


    # set '$RUN_PATH' to 
    # location of this script
    this_script=$(readlink -f "$0")
    RUN_PATH=$(dirname "$this_script")


    # where is source code?
    CODE_DIR="$USER_DIR/repos/biseps"


    # use which plato population folder?
    # PLATO_POP_DIR="$USER_DIR/data/github_plato/pop_output/field1/run_8" # centre square 1x1 degree
    # PLATO_POP_DIR="$ROOT_DIR/pop_output/field1/run_2" # centre square 1x1 degree
    PLATO_POP_DIR="/padata/beta/users/efarrell/data/old_platos/github_plato/pop_output/field1/run_8"
    # Do not forget to change back later when using own data!


    # location of SQLITE databases 
    # which holds binary eclipse depths
    DATABASE_DIR="$ROOT_DIR/jkt_output/run_2" 
    
    
    # Where will output files go?
    OUTPUT_DIR_BASE="$ROOT_DIR/eclipse_output"
    
    
    # Misc constants
    BINARY_FOLDERS='pop_b_'
    MAG_FILENAME='mag.dat.1'
    EXTRACT_FILENAME='extract'


    # how many times should we run the simulation?
    # NUM_SIMULATIONS=1
    NUM_SIMULATIONS=20


    # which files will we actually process?

    # e.g. just process grid square 16 only
    # target_pattern="*$BINARY_FOLDERS*16/$EXTRACT_FILENAME*"


    # process the entire grid
    target_pattern="*$BINARY_FOLDERS*$EXTRACT_FILENAME*"


    # Python programs needed for this script
    program_simulate="$CODE_DIR/plato_sims/sim_eclipses.py"
    program_analyse="$CODE_DIR/plato_sims/analyse_eclipses.py"

}



main() {


    # Store command-line arguments
    # for use in other functions
    COMMANDLINE_ARGS=("$@")


    # Create global variables pointing
    # to input / output directories
    setup_variables


    # Create a record for this job run
    save_job_details


    printf "\n\nSetting up Cluster Jobs. Please wait...\n"


    # jobs will be submitted in batches
    BATCH_NUM=1
    JOB_NUM=1


    # Find every 'extract' file in PLATO_POP_DIR
    # and create a cluster job array for each one
    find $PLATO_POP_DIR -iwholename "$target_pattern" -print | while read EXTRACT_FILE
    do

        # 'extract.1' files are stored 
        # in this directory structure:
        #
        # ...plato/pop_output/field1/run_6/POP_B_O/{1..100}/extract.1
        #                                     ↑       ↑         ↑
        #           $BINARY_TYPE  ------------↑       ↑         ↑
        #                                             ↑         ↑
        #                      $GRID_SQUARE ----------↑         ↑
        #                                                       ↑
        #                                        EXTRACT_FILE --↑



        # What path is 'extract' file in?
        INPUT_DIR=$(dirname "$EXTRACT_FILE")

        echo "Batch number is:  $BATCH_NUM"
        echo "Job number is:    $JOB_NUM"
        echo "Processing:       $INPUT_DIR"

        # Tip of path holds grid square
        GRID_SQUARE=$(basename "$INPUT_DIR")

        # next folder in path is either: 'pop_b_o' or 'pop_b_y' 
        BINARY_DIR=$(dirname "$INPUT_DIR")
        BINARY_TYPE=$(basename "$BINARY_DIR")

        # mag file is in same folder
        # as extract file
        MAG_FILE="$INPUT_DIR/$MAG_FILENAME"
                            
        # where is sqlite database?
        DATABASE="$DATABASE_DIR/$GRID_SQUARE/depths_$GRID_SQUARE.sqlite"


        OUTPUT_SQUARE_DIR="$RUN_OUTPUT/$GRID_SQUARE"

        # does an output folder exist 
        # already for this grid square?
        if [ ! -d "$OUTPUT_SQUARE_DIR" ]; then
            mkdir "$OUTPUT_SQUARE_DIR"
        fi


        # How many cluster jobs should we run?
        JOB_ARRAY="1-$NUM_SIMULATIONS:1"


        # create a name for the cluster job
        BATCH_NAME="sim$BATCH_NUM-sq$GRID_SQUARE"


        # launch 'sim_eclipses.sh' as a cluster job:
        job_ids=$(qsub -terse  -hold_jid "sim$BATCH_NUM*"       \
                    -v CODE_DIR="$CODE_DIR"                     \
                    -v RUN_OUTPUT="$RUN_OUTPUT"                 \
                    -v OUTPUT_SQUARE_DIR="$OUTPUT_SQUARE_DIR"   \
                    -v EXTRACT_FILE="$EXTRACT_FILE"             \
                    -v MAG_FILE="$MAG_FILE"                     \
                    -v DATABASE="$DATABASE"                     \
                    -v BINARY_TYPE="$BINARY_TYPE"               \
                    -N ${BATCH_NAME}                            \
                    -t ${JOB_ARRAY}                             \
                    -o "$CLUSTER_MSGS"                          \
                    sim_eclipses.sh)


        # setup jobs to run 
        # in concurrent batches
        (( JOB_NUM += 1 ))

        # should we start a 
        # new batch of jobs?
        if [ "$JOB_NUM" -gt 5 ];
        then
            # yes create new batch
            (( BATCH_NUM += 1 ))
            JOB_NUM=1
            echo "new batch: $BATCH_NUM"
        fi


    done


    # when all the simulations are done,
    # run a job which creates summary
    # histograms and calculates the mean
    # of the simulation data
    job2_name="analyse_eclipses"

    job2_ids=$(qsub -terse -hold_jid 'sim*'          \
               -v CODE_DIR="$CODE_DIR"               \
               -v RUN_OUTPUT="$RUN_OUTPUT"           \
               -N "$job2_name"                       \
               -o "$CLUSTER_MSGS"                    \
               analyse_eclipses.sh)



    # Helpful info for user
    printf "\nAll output from this job will be in folder: \n%s" ${RUN_OUTPUT}
    printf "\n\nUse 'qstat' to check the progress of this job.\n"

}



setup_variables() {

    # command line argument:
    # Short comment explaining purpose of run.
    COMMENT=${COMMANDLINE_ARGS[0]}


    # most important step
    # define input files etc
    define_inputs


    # Activate useful bash functions. If you
    # cant find a function mentioned in these
    # scripts, its probably in "utility_functions.sh"
    source "$CODE_DIR/common/utility_functions.sh"


    # where does output go?
    create_output_folder


    # double-check everything exists
    assert_folder_exists  "USER_DIR"                "$USER_DIR"  
    assert_folder_exists  "ROOT_DIR"                "$ROOT_DIR"  
    assert_folder_exists  "PLATO_POP_DIR"           "$PLATO_POP_DIR"  
    assert_folder_exists  "DATABASE_DIR"            "$DATABASE_DIR"   
    assert_folder_exists  "RUN_PATH"                "$RUN_PATH"       
    assert_folder_exists  "CODE_DIR"                "$CODE_DIR"       
    assert_folder_exists  "RUN_OUTPUT"              "$RUN_OUTPUT"     
    assert_folder_exists  "CLUSTER_MSGS"            "$CLUSTER_MSGS"   

    assert_file_exists    "program_simulate"        "$program_simulate"   
    assert_file_exists    "program_analyse"         "$program_analyse"   


    # copy the Python programs
    # to the runtime directory
    cp "$program_simulate"      "$RUN_OUTPUT"
    cp "$program_analyse"       "$RUN_OUTPUT"

}



create_output_folder() {


    # make sure base Output directory exists 
    if [ ! -d "$OUTPUT_DIR_BASE" ]; then 
        mkdir "$OUTPUT_DIR_BASE" --parents
    fi
       
    # Create a new directory to hold 
    # all output from this particular run. 
    RUN_OUTPUT=$(increment_folder "$OUTPUT_DIR_BASE"/run_)
    CLUSTER_MSGS="$RUN_OUTPUT/cluster_msgs"

    mkdir "$RUN_OUTPUT"
    mkdir "$CLUSTER_MSGS"
 
}



save_job_details () {

    # Store info about this job in a log file

    JOB_INFO="$RUN_OUTPUT/job_info.txt"
    LOG_FILE="$RUN_PATH/log.txt"


    echo_job_time                                               >> "$JOB_INFO"
    echo_value  'RUN_OUTPUT'         "${RUN_OUTPUT}"            >> "$JOB_INFO"
    echo_value  'COMMENT'            "${COMMENT}"               >> "$JOB_INFO"
    echo_value  'RUN_PATH'           "${RUN_PATH}"              >> "$JOB_INFO"
    echo_value  'CODE_DIR'           "${CODE_DIR}"              >> "$JOB_INFO"
    echo_value  'PLATO_POP_DIR'      "${PLATO_POP_DIR}"         >> "$JOB_INFO"
    echo_value  'DATABASE_DIR'       "${DATABASE_DIR}"          >> "$JOB_INFO"
    echo_value  'BINARY_FOLDERS'     "${BINARY_FOLDERS}"        >> "$JOB_INFO"
    echo_value  'MAG_FILENAME'       "${MAG_FILENAME}"          >> "$JOB_INFO"
    echo_value  'EXTRACT_FILENAME'   "${EXTRACT_FILENAME}"      >> "$JOB_INFO"
    echo_value  'NUM_SIMULATIONS'    "${NUM_SIMULATIONS}"       >> "$JOB_INFO"

    insert_log  "${COMMENT}"  "${RUN_OUTPUT}"                   >> "$LOG_FILE"

    show_git_status    "$CODE_DIR/plato_sims"                   >> "$JOB_INFO" 
    show_git_status    "$CODE_DIR/common"                       >> "$JOB_INFO" 


    # make a copy of all files
    # current in the main directory
    find "$RUN_PATH" -maxdepth 1 -type f -name '*' -exec cp "{}" "$RUN_OUTPUT" \;

    # make an extra copy of the log file
    cp "$LOG_FILE"   "$OUTPUT_DIR_BASE"

    # record all the 'extract' 
    # files we're going to process
    printf "\nThe following files will be processed:\n"         >> "$JOB_INFO" 
    find $PLATO_POP_DIR -iwholename "$target_pattern" -print    >> "$JOB_INFO"

}



# Validate command-line arguments
if [ "$#" -ne 1 ];
then
    echo "Error! Command-line argument missing."
    echo "Please supply a comment (in quotes) explaining the purpose of this job"
    exit 1
fi


# Kick off the job
main "$@"




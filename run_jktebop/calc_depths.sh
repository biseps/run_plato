#!/bin/bash

#$ -l s_rt=100:00:00,virtual_free=6G,h_vmem=6G
#$ -M pam.rowden27@gmail.com
#$ -m as
#$ -P science
#$ -S /bin/bash
#$ -j y
#$ -cwd



# get utility functions.
# This file is important and contains 
# bash functions used throughout this script.
source "$CODE_DIR/common/utility_functions.sh"


# define executables
program_coeffs="$OUTPUT_DIR/magLdcGd.py"
program_depths="$OUTPUT_DIR/jktebopGen"


# split extract files are numbered ".000", ".001", ".002" etc..
# but $SGE_TASK_ID starts from "1", 
# so fix off-by-one error and pad with zeros.
SUFFIX=$( printf "%0.3d" $(($SGE_TASK_ID - 1)) )

EXTRACT_FILE="$INPUT_DIR/$SPLIT_FILENAME.$SUFFIX"

echo_value  'file suffix  (SGE_TASK_ID): ' "$SUFFIX"
echo_value  'INPUT_DIR'                    "$INPUT_DIR"       
echo_value  'OUTPUT_SQUARE_DIR'            "$OUTPUT_SQUARE_DIR"      
echo_value  'BINARY_TYPE'                  "$BINARY_TYPE"     


# validate  everything
assert_folder_exists  "INPUT_DIR"         "$INPUT_DIR"         
assert_folder_exists  "OUTPUT_SQUARE_DIR" "$OUTPUT_SQUARE_DIR" 
assert_folder_exists  "CODE_DIR"          "$CODE_DIR"          

assert_file_exists    "program_coeffs"    "$program_coeffs" 
assert_file_exists    "program_depths"    "$program_depths" 
assert_file_exists    "EXTRACT_FILE"      "$EXTRACT_FILE"   



# Setup Python 
# runtime environment
set_python_env  anaconda  $CODE_DIR


# create gravity and 
# limb darkening coefficients
debug_msg "starting... $program_coeffs"

$PYTHONBIN "$program_coeffs"  "$EXTRACT_FILE"  > "$OUTPUT_SQUARE_DIR/coeff.$BINARY_TYPE.$SUFFIX"

debug_msg "finished $program_coeffs"



# were coefficients created?
assert_file_exists    "coefficient file:"   "$OUTPUT_SQUARE_DIR/coeff.$BINARY_TYPE.$SUFFIX" 



# generate binary eclipse depths 
debug_msg "starting... $program_depths"

"$program_depths" "$EXTRACT_FILE"  "$OUTPUT_SQUARE_DIR/coeff.$BINARY_TYPE.$SUFFIX"  > "$OUTPUT_SQUARE_DIR/eclipse_depths.$BINARY_TYPE.$SUFFIX"

debug_msg "finished... $program_depths"


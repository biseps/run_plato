#!/bin/bash

# Purpose:

# Find all sqlite databases that have been created
# by 'db_load.sh' and add an 'index' to them.

# An 'index' will make the database run much quicker.


#$ -l s_rt=100:00:00,virtual_free=3G,h_vmem=6G
#$ -t 1-1:1
#$ -S /bin/bash
#$ -j y
#$ -cwd
#$ -P science
#$ -M pam.rowden27@gmail.com
#$ -m as


TABLE_NAME='depths'

target_pattern="*depths*sqlite"

# if manual run:
# RUN_OUTPUT='./output/run_1/'


# find every single SQLITE database 
# and insert an index...
find "$OUTPUT_DIR" -iname "$target_pattern" | while read DB_FILE
do
    printf "\n\nProcessing: %s\n" "$DB_FILE"

$PROGRAM_SQLITE  "$DB_FILE" <<EOF

.echo on          
begin transaction;
create index  idx_num     on ${TABLE_NAME} (num);
create index  idx_main    on ${TABLE_NAME} (id, inclin, z);
commit;

.indices ${TABLE_NAME}
EOF

done



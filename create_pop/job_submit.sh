#!/bin/bash


define_paths() {

    # setup most basic locations
    USER_DIR="/padata/beta/users/pmr257"
    ROOT_DIR="$USER_DIR/data/plato"


    # set '$RUN_PATH' to 
    # location of this script
    this_script=$(readlink -f "$0")
    RUN_PATH=$(dirname "$this_script")


    # location of BISEPS 
    # stellar evolutionary tracks 
    BISEPS_DIR="$USER_DIR/data/biseps2_runs/run_biseps"


    # Each stellar field is divided into
    # grid squares. Specify how much of 
    # the grid to generate...
    # (Note: 'create_popin_files.py' creates the grid)

    # GRID_START=1
    # GRID_END=50

    GRID_START=16
    GRID_END=16


    # git repositories with source code
    CODE_DIR="$USER_DIR/repos/biseps"
               

    # which plato field?
    FIELD_DIR="field1"

                          
    # location of "pop.in" files
    # for this particular field
    POP_PARAMS_DIR="$RUN_PATH/pop_input/pams_first_run" 


    # Where will output go?
    OUTPUT_BASE_DIR="$ROOT_DIR/pop_output/$FIELD_DIR"


    # Fortran/Python programs used
    program_pop="$CODE_DIR/pop/population"         
    program_ranloc="$CODE_DIR/scripts/ranLoc"         
    program_extract="$CODE_DIR/scripts/extract"        
    program_mag="$CODE_DIR/mag/makeAllMags.py"     

}



main() {


    # Store command-line arguments in global
    # array for other functions to use
    COMMANDLINE_ARGS=("$@")


    # Create global variables pointing
    # to input / output directories etc
    setup_variables


    # make a record of all 
    # parameters and settings
    store_job_info


    # create number of cluster tasks based
    # on how many squares we're doing
    JOB_ARRAY="$GRID_START-$GRID_END:1"


    # cluster job name
    JOB_NAME="pla-$GRID_START-$GRID_END"


    # use 'qsub' to launch cluster job
    job_ids=$(qsub -terse                           \
              -v VER=${VER}                         \
              -v CODE_DIR=${CODE_DIR}               \
              -v BISEPS_DIR=${BISEPS_DIR}           \
              -v OUTPUT_DIR=${OUTPUT_DIR}           \
              -N ${JOB_NAME}                        \
              -t ${JOB_ARRAY}                       \
              -o ${OUTPUT_DIR}                      \
              create_pop.clus.sh)


    # Helpful info for user
    printf "\nAll output from this job will be in folder: \n%s" ${OUTPUT_DIR}
    printf "\n\nUse 'qstat' to check on progress of this job.\n"

}



setup_variables() {


    # Short comment explaining purpose of run.
    COMMENT=${COMMANDLINE_ARGS[0]}


    # The version .NUMBER at end of all files.
    VER=1


    # location of source code
    # and input/output data files
    define_paths


    # Activate useful bash functions. If you
    # cant find a function mentioned in these
    # scripts, its probably in "utility_functions.sh"
    source "$CODE_DIR/common/utility_functions.sh"


    # put output where?
    create_output_dir
    assert_folder_exists  "OUTPUT_DIR"  "$OUTPUT_DIR" 


    # Does everything exist?
    assert_folder_exists  "USER_DIR"        "$USER_DIR" 
    assert_folder_exists  "ROOT_DIR"        "$ROOT_DIR" 
    assert_folder_exists  "RUN_PATH"        "$RUN_PATH" 
    assert_folder_exists  "BISEPS_DIR"      "$BISEPS_DIR" 
    assert_folder_exists  "CODE_DIR"        "$CODE_DIR" 
    assert_folder_exists  "POP_PARAMS_DIR"  "$POP_PARAMS_DIR" 


    # make sure the programs exist
    assert_file_exists  "program_pop"     "$program_pop"     
    assert_file_exists  "program_ranloc"  "$program_ranloc"  
    assert_file_exists  "program_extract" "$program_extract" 
    assert_file_exists  "program_mag"     "$program_mag"     


    # copy all 'pop.in' files
    # to the output directory for this run
    cp -R ${POP_PARAMS_DIR}/* ${OUTPUT_DIR}/


    # copy the Fortran/Python programs 
    # to the output folder
    cp "$program_pop"       "$OUTPUT_DIR"  
    cp "$program_ranloc"    "$OUTPUT_DIR"  
    cp "$program_extract"   "$OUTPUT_DIR"  
    cp "$program_mag"       "$OUTPUT_DIR"  



}



create_output_dir() {


    # make sure base Output directory exists 
    if [ ! -d "$OUTPUT_BASE_DIR" ]; then 
        mkdir "$OUTPUT_BASE_DIR" --parents
    fi


    # Create actual output folder e.g. 'run_1', 'run_2' etc.
    # All output from this job run will be stored here.
    OUTPUT_DIR=$(increment_folder "$OUTPUT_BASE_DIR/run_")
    mkdir ${OUTPUT_DIR}

}



store_job_info() {

    # Write details of this run
    # to a text file

    JOB_INFO="$OUTPUT_DIR/job_info.txt"
    LOG_FILE="$RUN_PATH/log.txt"

    echo_job_time                                           >> "$JOB_INFO"

    echo_value  'COMMENT'           "${COMMENT}"            >> "$JOB_INFO"
    echo_value  'USER_DIR'          "${USER_DIR}"           >> "$JOB_INFO"
    echo_value  'ROOT_DIR'          "${ROOT_DIR}"           >> "$JOB_INFO"
    echo_value  'COMMENT'           "${COMMENT}"            >> "$JOB_INFO"
    echo_value  'FIELD_DIR'         "${FIELD_DIR}"          >> "$JOB_INFO"
    echo_value  'OUTPUT_DIR'        "${OUTPUT_DIR}"         >> "$JOB_INFO"
    echo_value  'VER'               "${VER}"                >> "$JOB_INFO"
    echo_value  'RUN_PATH'          "${RUN_PATH}"           >> "$JOB_INFO"
    echo_value  'BISEPS_DIR'        "${BISEPS_DIR}"         >> "$JOB_INFO"
    echo_value  'POP_PARAMS_DIR'    "${POP_PARAMS_DIR}"     >> "$JOB_INFO"
    echo_value  'CODE_DIR'          "${CODE_DIR}"           >> "$JOB_INFO"

    show_git_status    "$CODE_DIR/pop"                      >> "$JOB_INFO" 
    show_git_status    "$CODE_DIR/mag"                      >> "$JOB_INFO" 
    show_git_status    "$CODE_DIR/plot"                     >> "$JOB_INFO" 
    show_git_status    "$CODE_DIR/scripts"                  >> "$JOB_INFO" 
    show_git_status    "$CODE_DIR/common"                   >> "$JOB_INFO" 

    insert_log  "${COMMENT}"     "${OUTPUT_DIR}"            >> "$LOG_FILE"
   

    # make a copy of all scripts used
    find "$RUN_PATH" -maxdepth 1 -type f -name '*' -exec cp "{}" "$OUTPUT_DIR" \;


    # make a few copies of the log file
    cp "$LOG_FILE"  "$OUTPUT_DIR"
    cp "$LOG_FILE"  "$OUTPUT_BASE_DIR"

}



# Validate command-line arguments
if [ "$#" -ne 1 ];
then
    echo "Error! Command-line argument missing."
    echo "Please supply a comment (in quotes) explaining the purpose of this job"
    exit 1
fi



# Kick off the job
main "$@"




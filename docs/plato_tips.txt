
centre field 1x1 degree 
    centre stripe
    square 16

centre field 1x1 degree 
    full square
    square 776

centre field 5x5 degree
    square 36

The two long-duration PLATO 2.0 fields form the core of the mission.

Their centres must stay within two regions imposed by observability
constraints. These “allowed regions” are spherical caps defined by an
ecliptic latitude |β|>63, and are located respectively in the southern
and northern hemispheres, mostly at high declinations (|δ|>40). 

The choice of the long-duration fields is driven by the need to find
a trade-off between the number of priority targets and a minimum rate
of false-alarms due to crowding.

A proposed conservative choice (to minimize contaminants, still
satisfying the scientific requirements in terms of target numbers) for
field centres is

(l=65,  b=30)  -  Northern sky field
(l=253, b=-30) -  Southern sky field

These fields are centred approximately on the Lyra/Hercules
(North) and Pictor (South) constellations. The northern field
includes the Kepler field on a corner. 

An additional, thorough study of the contaminant problem will allow us
to verify whether the field centre can be moved to lower Galactic
latitudes (|b|~25), thus increasing the number of targets.

